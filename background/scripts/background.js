// **** Token Implicit Grant (Browser) - UserLogin ****

const platformClient = require('platformClient');
const client = platformClient.ApiClient.instance;
client.setPersistSettings(true);
client.setEnvironment('mypurecloud.ie');


let datatableId = 'cfca808f-777f-4e69-9274-114d26e6503e';
var responses;
var activeQueue;
var pcQueues = [];
let pcUserId;


function findPCToken() {
    console.log('init find PureCloudToken timer...');
    let refreshIntervalId = setInterval(function () {

        chrome.tabs.query({ currentWindow: true, active: true },
            function (activeTab) {
                if (activeTab[0].url.includes('https://apps.mypurecloud')) {
                    console.log('PureCloud App active, get Token...');
                    chrome.tabs.executeScript(null, {
                        code: 'localStorage.getItem("pc_auth");'
                    },
                        function (resp) {
                            if (resp) {

                                let pcTOKEN = JSON.parse(resp).authenticated.access_token;
                                clearInterval(refreshIntervalId);
                                console.log('Token retrieved, invalidate timer.');
                                client.setAccessToken(pcTOKEN);
                                initPureCloud(pcTOKEN);


                            }

                        });
                }
            }
        );

    }, 2000);

}

findPCToken();

function initPureCloud(aToken) {
    

    // GetMe
    var apiInstance = new platformClient.UsersApi();
    apiInstance.getUsersMe()
        .then((data) => {
            console.log(`getUsersMe success! UserId: ${data.id}`);
            pcUserId = data.id;
            // GetQueues

            apiInstance = new platformClient.RoutingApi();
            var opts = {
                'pageSize': 100, // Number | Page size
                'pageNumber': 1, // Number | Page number
                'sortBy': "name", // String | Sort by
                'active': true
            };

            apiInstance.getRoutingQueues(opts)
                .then(function (resp) {
                    resp.entities.forEach(function (item) {
                        pcQueues[item.id] = item.name;
                            
                    });

                    // GetCannedResponses
                    apiInstance = new platformClient.ArchitectApi();
                    let opts = {
                        'pageNumber': 1, // Number | Page number
                        'pageSize': 50, // Number | Page size
                        'showbrief': false // Boolean | If true returns just the key value of the row
                    };

                    apiInstance.getFlowsDatatableRows(datatableId, opts)
                        .then((data) => {
                            console.log(`getFlowsDatatableRows success! data: ${JSON.stringify(data, null, 2)}`);
                            responses = data.entities;
                            subscribeForEvents(aToken);
                            

                        })
                        .catch((err) => {
                            console.log('There was a failure calling getFlowsDatatableRows');
                            console.error(err);
                        });
                })
                .catch(function (err) {
                    console.log('There was a failure calling getRoutingQueues');
                    console.error(err);
                });
        })
        .catch((err) => {
            console.log('There was a failure calling getUsersMe');
            console.error(err);
        });


}




function subscribeForEvents(aToken) {
    console.log('Try to create channel for notifications...');

    var apiInstance = new platformClient.NotificationsApi;


    apiInstance.postNotificationsChannels().then(function (result) {
        //successful call
        console.log("success");
        console.log(result);

        var _webSocket = new WebSocket(result.connectUri);
        var _channelID = result.id;

        _webSocket.onopen = function () {
            console.log("socket opened");


            var body = [{
                "id": "v2.users." + pcUserId + ".conversations.emails"
            }];

            apiInstance.postNotificationsChannelSubscriptions(_channelID, body).then(function (data) {
                //successful call
                console.log(data);



            }).catch(function (error) {
                //error while making call
                console.log(error);
            });

        }

        _webSocket.onmessage = function (message) {

            var json = JSON.parse(message.data);
            if (json.topicName !== 'channel.metadata') {
                console.log(json);

                // Get status for Connected Interaction
                // Get last Participant details
                try {
                    let lastParticipant = json.eventBody.participants[json.eventBody.participants.length - 1];

                    if (lastParticipant.purpose.toLowerCase() == 'agent' && lastParticipant.state.toLowerCase() == 'connected' && !lastParticipant.held) {
                        activeQueue = pcQueues[lastParticipant.queue.id];
                        console.log(`origin queueId for selected interaction: ${activeQueue}`);

                    }
                } catch (error) {
                    console.error(error);
                }


            }


        };

        _webSocket.onclose = function (message) {
            //
        };

        _webSocket.onerror = function (message) {
            //
        };


    }).catch(function (error) {
        //error while making call
        console.log(error);
    });



}