popupApp.controller('popupController', function ($scope) {

  console.log('loading popupController');
  $scope.pageTitle = "Responses";

  var background = chrome.extension.getBackgroundPage();

  $scope.myResponses = background.responses;
  $scope.activeQueue = background.activeQueue;


  function buildInjectScript(htmlBody) {
    return `var myJavaScript = \"tinymce.execCommand('mceInsertContent', '', '${htmlBody}') \"; var scriptTag = document.createElement('script'); scriptTag.innerHTML = myJavaScript; document.head.appendChild(scriptTag); `;
  }

  $scope.insertObject = function (cannedResponse) {


    cannedResponse = cannedResponse.replace(/[\r\n]/g, "");
    cannedResponse = cannedResponse.replace(/["]/g, "\\\"");


    chrome.tabs.executeScript(null, {
      code: buildInjectScript(cannedResponse)

    },
      function () {
      });

  }


});